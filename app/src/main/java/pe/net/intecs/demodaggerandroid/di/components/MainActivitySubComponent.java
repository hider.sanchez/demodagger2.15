/*
 * Created by Hider Sanchez 01/10/18 03:42 AM
 * Copyright (c) 2018. Intecs.pe All rights reserved.
 */

package pe.net.intecs.demodaggerandroid.di.components;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import pe.net.intecs.demodaggerandroid.di.modules.MainActivityModule;
import pe.net.intecs.demodaggerandroid.ui.activities.MainActivity;

@Subcomponent(modules={MainActivityModule.class})
public interface MainActivitySubComponent extends AndroidInjector<MainActivity> {
   
   @Subcomponent.Builder
   abstract class Builder extends AndroidInjector.Builder<MainActivity> {
   }
   
}
