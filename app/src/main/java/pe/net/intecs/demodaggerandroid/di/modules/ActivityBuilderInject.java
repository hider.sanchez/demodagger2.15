/*
 * Created by Hider Sanchez 01/10/18 03:38 AM
 * Copyright (c) 2018. Intecs.pe All rights reserved.
 */

package pe.net.intecs.demodaggerandroid.di.modules;

import android.app.Activity;

import dagger.Binds;
import dagger.Module;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjector;
import dagger.multibindings.IntoMap;
import pe.net.intecs.demodaggerandroid.di.components.MainActivitySubComponent;
import pe.net.intecs.demodaggerandroid.ui.activities.MainActivity;

@Module(subcomponents={MainActivitySubComponent.class})
public abstract class ActivityBuilderInject {
   
   @Binds
   @IntoMap
   @ActivityKey(MainActivity.class)
   abstract AndroidInjector.Factory<? extends Activity> bindMainActivity(MainActivitySubComponent.Builder builder);
}
