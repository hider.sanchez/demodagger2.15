/*
 * Created by Hider Sanchez 01/10/18 05:09 AM
 * Copyright (c) 2018. Intecs.pe All rights reserved.
 */

package pe.net.intecs.demodaggerandroid.ui.main;

public interface MainPresenterContract {
   void showView();
}
