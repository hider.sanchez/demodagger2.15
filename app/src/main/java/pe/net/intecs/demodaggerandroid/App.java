/*
 * Created by Hider Sanchez 01/10/18 03:36 AM
 * Copyright (c) 2018. Intecs.pe All rights reserved.
 */

package pe.net.intecs.demodaggerandroid;

import android.app.Activity;
import android.app.Application;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import pe.net.intecs.demodaggerandroid.di.components.DaggerAppComponent;

public class App extends Application implements HasActivityInjector {
   @Inject
   DispatchingAndroidInjector<Activity> mActivityDispatchingAndroidInjector;
   
   @Override
   public void onCreate() {
      
      super.onCreate();
      onCreateDependencyInjection();
   }
   
   @Override
   public AndroidInjector<Activity> activityInjector() {
      return mActivityDispatchingAndroidInjector;
   }
   
   private void onCreateDependencyInjection() {
      DaggerAppComponent.builder().aplication(this).build().inject(this);
   }
}
