/*
 * Created by Hider Sanchez 01/10/18 03:45 AM
 * Copyright (c) 2018. Intecs.pe All rights reserved.
 */

package pe.net.intecs.demodaggerandroid.di.modules;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import pe.net.intecs.demodaggerandroid.entity.User;
import pe.net.intecs.demodaggerandroid.ui.main.MainPresenter;
import pe.net.intecs.demodaggerandroid.ui.main.MainPresenterContract;

@Module
public class MainActivityModule {
   
   @Provides
   public User provideUser() {
      return new User("Hider", "Sanchez");
   }
   

}
