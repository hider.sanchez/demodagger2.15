/*
 * Created by Hider Sanchez 01/10/18 03:19 AM
 * Copyright (c) 2018. Intecs.pe All rights reserved.
 */

package pe.net.intecs.demodaggerandroid

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
   @Test
   fun useAppContext() {
      // Context of the app under test.
      val appContext = InstrumentationRegistry.getTargetContext()
      assertEquals("pe.net.intecs.demodaggerandroid", appContext.packageName)
   }
}
